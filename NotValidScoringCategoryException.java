/**
 * Created by cheny2 on 1/13/17.
 */
public class NotValidScoringCategoryException extends RuntimeException {

    public NotValidScoringCategoryException(String message) {
        super(message);
    }
}
