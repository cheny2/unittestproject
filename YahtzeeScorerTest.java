import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * This class tests the functions of the YahtzeeScorer using
 * unit tests.
 *
 * @author: Yitong Chen
 */
public class YahtzeeScorerTest {

    private YahtzeeScorer yahtzeeScorer1;

    /* Sets up the YahtzeeScorer object used in each test;*/
    @Before
    public void setUp() throws Exception {
        yahtzeeScorer1 = new YahtzeeScorer();
    }

    /* Tests whether the user can create a YahtzeeScorer;*/
    @Test
    public void canCreateClass() {
        YahtzeeScorer yahtzeeScorer1 = new YahtzeeScorer();
    }

    /* Tests whether the YahtzeeScorer can return the current score of the user. */
    @Test
    public void canReturnCurrentScore() {
        assertEquals(0, yahtzeeScorer1.getScore());
    }

    /* Tests whether the score could be set to a given value. */
    @Test
    public void canSetScore() {
        yahtzeeScorer1.setScore(20);
        assertEquals(20, yahtzeeScorer1.getScore());
    }

    /* Tests whether a method to evaluate the dices exists in the YahtzeeScorer class. */
    @Test
    public void canEvaluate() {
        int[] dice = {5, 5, 5, 5, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice);
    }

    /* Tests the Normal Play scenario for a Yahtzee scoring. */
    @Test
    public void yahtzeeReturnsNormal() {
        int[] dice = {5, 5, 5, 5, 5};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Yahtzee", dice));
        assertEquals(50, yahtzeeScorer1.getScore());
    }

    /* Tests the Insufficient Dice scenario for a Yahtzee scoring. */
    @Test
    public void yahtzeeReturnInsufficient() {
        int[] dice = {5, 5, 5, 5, 4};
        assertEquals("Insufficient Dice", yahtzeeScorer1.evaluate("Yahtzee", dice));
        assertEquals(0, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * The rule allows the user to score multiple Yahtzees.
     *
     * If the user already has a Yahtzee, s/he
     * receives a bonus 100 points for every additional Yahtzee roll
     * and a joker. The joker allows the user to choose to score a Yahtzee, or a Large Straight,
     * or a Full House, or a Three Of A Kind with the additional Yahtzee roll and gets
     * the corresponding points in addition to the 100 points of bonus.
     *
     * If the user has already put 0 in the Yahtzee box, s/he doesn't get
     * the 100 point bonus but still gets a joker.
     *
     * This test deals with the scenario where the user chooses to score
     * a second Yahtzee with the second Yahtzee roll and
     * s/he already has a Yahtzee. As a result, the
     * expected score becomes 50 + 100 + 50 = 200;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeAlreadyHaveYahtzeeScoreYahtzee() {
        int[] dice1 = {5, 5, 5, 5, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Yahtzee", dice2));
        assertEquals(200, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Large Straight with the second Yahtzee roll and
     * s/he already has a Yahtzee. As a result, the
     * expected score becomes 50 + 100 + 40 = 190;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeAlreadyHaveYahtzeeScoreLargeStraight() {
        int[] dice1 = {5, 5, 5, 5, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Large Straight", dice2));
        assertEquals(190, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Full House with the second Yahtzee roll and
     * s/he already has a Yahtzee. As a result, the
     * expected score becomes 50 + 100 + 25 = 175;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeAlreadyHaveYahtzeeScoreFullHouse() {
        int[] dice1 = {5, 5, 5, 5, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Full House", dice2));
        assertEquals(175, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Three Of A Kind with the second Yahtzee roll and
     * s/he already has a Yahtzee. As a result, the
     * expected score becomes 50 + 100 + score(Three Of A Kind) = 165;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeAlreadyHaveYahtzeeScoreThreeOfAKind() {
        int[] dice1 = {5, 5, 5, 5, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Three Of A Kind", dice2));
        assertEquals(165, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Yahtzee with the second Yahtzee roll and
     * s/he already has zero for Yahtzee. As a result, the
     * expected score becomes 50;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeHaveZeroScoreYahtzee() {
        int[] dice1 = {5, 5, 5, 4, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Yahtzee", dice2));
        assertEquals(50, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Large Straight with the second Yahtzee roll and
     * s/he already has zero for Yahtzee. As a result, the
     * expected score becomes 40;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeHaveZeroScoreLargeStraight() {
        int[] dice1 = {5, 5, 5, 4, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Large Straight", dice2));
        assertEquals(40, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Full House with the second Yahtzee roll and
     * s/he already has zero for Yahtzee. As a result, the
     * expected score becomes 25;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeHaveZeroScoreFullHouse() {
        int[] dice1 = {5, 5, 5, 4, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Full House", dice2));
        assertEquals(25, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Yahtzee scoring.
     * This test deals with the scenario where the user chooses to score
     * a Three Of A Kind with the second Yahtzee roll and
     * s/he already has zero for Yahtzee. As a result, the
     * expected score becomes score(Three Of A Kind) = 15;
     */
    @Test
    public void yahtzeeReturnMultipleYahtzeeHaveZeroScoreThreeOfAKind() {
        int[] dice1 = {5, 5, 5, 4, 5};
        yahtzeeScorer1.evaluate("Yahtzee", dice1);
        int[] dice2 = {3, 3, 3, 3, 3};
        assertEquals("Multiple Yahtzee!", yahtzeeScorer1.evaluate("Three Of A Kind", dice2));
        assertEquals(15, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Large Straight score.
     * The dice values are sorted in ascending order.
     */
    @Test
    public void largeStraightOrderedReturnNormal() {
        int[] dice = {1, 2, 3, 4, 5};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Large Straight", dice));
        assertEquals(40, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Large Straight score.
     * The dice values are unsorted.
     */
    @Test
    public void largeStraightUnorderedReturnNormal() {
        int[] dice = {4, 3, 2, 5, 6};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Large Straight", dice));
        assertEquals(40, yahtzeeScorer1.getScore());
    }

    /* Tests the Insufficient Dice scenario for a Large Straight score.
     */
    @Test
    public void largeStraightReturnInsufficient() {
        int[] dice = {3, 2, 6, 5, 1};
        assertEquals("Insufficient Dice", yahtzeeScorer1.evaluate("Large Straight", dice));
        assertEquals(0, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Large Straight score.
     * Attempt to score the same category more than once should throw an exception.
     */
    @Test
    public void largeStraightThrowAlreadyScoredException() {
        int[] dice1 = {1, 2, 4, 5, 3};
        yahtzeeScorer1.evaluate("Large Straight", dice1);
        int[] dice2 = {5, 2, 3, 6, 4};
        try {
            yahtzeeScorer1.evaluate("Large Straight", dice2);
            fail("This category has already been scored!");
        } catch (AlreadyScoredException success) {
            //if we caught the exception, this is success
        }
        assertEquals(40, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Full House score.
     * The dices are sorted in ascending order.
     */
    @Test
    public void fullHouseNormalOrderedReturnNormal() {
        int[] dice = {2, 2, 2, 3, 3};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Full House", dice));
        assertEquals(25, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Full House score.
     * The dices are unsorted.
     */
    @Test
    public void fullHouseUnorderedReturnNormal() {
        int[] dice = {3, 4, 3, 4, 4};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Full House", dice));
        assertEquals(25, yahtzeeScorer1.getScore());
    }

    /* Tests the Insufficient Dice scenario for a Full House score.
     */
    @Test
    public void fullHouseReturnInsufficient() {
        int[] dice = {3, 2, 6, 5, 1};
        assertEquals("Insufficient Dice", yahtzeeScorer1.evaluate("Full House", dice));
        assertEquals(0, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Full House score.
     * Attempt to score the same category more than once should throw an exception.
     */
    @Test
    public void fullHouseReturnAlreadyScored() {
        int[] dice1 = {2, 2, 2, 3, 3};
        yahtzeeScorer1.evaluate("Full House", dice1);
        int[] dice2 = {3, 4, 3, 4, 4};
        try {
            yahtzeeScorer1.evaluate("Full House", dice2);
            fail("This category has already been scored!");
        } catch (AlreadyScoredException success) {
            //if we caught the exception, this is success!
        }
        assertEquals(25, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Three-of-a-kind score.
     * The dices are sorted in ascending order.
     */
    @Test
    public void threeOfAKindOrderedReturnNormal() {
        int[] dice = {2, 2, 2, 4, 5};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Three Of A Kind", dice));
        assertEquals(15, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Three-of-a-kind score.
     * The dices are sorted in ascending order.
     * Since the points a user score with a Three-of-a-kind roll depend
     * on the actual dices, this test pass in a different array of dice values
     * from the previous test.
     */
    @Test
    public void threeOfAKindOrderedDifferentNumbersReturnNormal() {
        int[] dice = {3, 3, 3, 4, 5};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Three Of A Kind", dice));
        assertEquals(18, yahtzeeScorer1.getScore());
    }

    /* Tests the Normal Play scenario for a Three-of-a-kind score.
     * The dices are unsorted.
     */
    @Test
    public void threeOfAKindUnorderedReturnNormal() {
        int[] dice = {2, 1, 2, 3, 2};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Three Of A Kind", dice));
        assertEquals(10, yahtzeeScorer1.getScore());
    }

    /* Tests the Insufficient Dice scenario for a Three-of-a-kind score. */
    @Test
    public void threeOfAKindReturnInsufficient() {
        int[] dice = {3, 2, 6, 5, 1};
        assertEquals("Insufficient Dice", yahtzeeScorer1.evaluate("Three Of A Kind", dice));
        assertEquals(0, yahtzeeScorer1.getScore());
    }

    /* Tests the Already Scored scenario for a Three-of-a-kind score.
       Attempt to score the same category more than once should throw an exception.
     */
    @Test
    public void threeOfAKindReturnAlreadyScored() {
        int[] dice1 = {2, 2, 2, 4, 5};
        yahtzeeScorer1.evaluate("Three Of A Kind", dice1);
        int[] dice2 = {3, 4, 1, 4, 4};
        try {
            yahtzeeScorer1.evaluate("Three Of A Kind", dice2);
            fail("This category has already been scored!");
        } catch (AlreadyScoredException success) {
            //if we caught the exception, this is success!
        }
        assertEquals(15, yahtzeeScorer1.getScore());
    }

    /* Tests scoring a Three-of-a-kind using a Full House roll.
     * Since Full House is a subset of Three-of-a-kind, the user
     * should be allowed to score normally.
     */
    @Test
    public void fullHouseScoreThreeOfAKindReturnNormal() {
        int[] dice = {2, 2, 2, 4, 4};
        assertEquals("Normal Play", yahtzeeScorer1.evaluate("Three Of A Kind", dice));
        assertEquals(14, yahtzeeScorer1.getScore());
    }

    /*
     * The following tests concerns invalid inputs.
     * This one tests if the user passes in an invalid scoring category.
     */
    @Test
    public void notValidCategoryThrowsException() {
        int[] dice = {2, 3, 1, 2, 4};
        try {
            yahtzeeScorer1.evaluate("Two Of A Kind", dice);
            fail("Should not be able to score a non-exist category");
        } catch (NotValidScoringCategoryException success) {
            //if we caught the exception, this is success!
        }
    }

    /*
     * Tests if the user passes in an array of fewer than five dice values.
     */
    @Test
    public void notEnoughDiceThrowsException() {
        int[] dice = {2, 3, 3, 2};
        try {
            yahtzeeScorer1.evaluate("Yahtzee", dice);
            fail("Should not be able to score with fewer than five dices");
        } catch (NotEnoughDiceException success) {
            //if we caught the exception, this is success!
        }
    }

    /*
     * Tests if the user passes in an array of more than five dice values.
     */
    @Test
    public void tooManyDiceThrowsException() {
        int[] dice = {1, 2, 3, 4, 5, 6};
        try {
            yahtzeeScorer1.evaluate("Yahtzee", dice);
            fail("Should not be able to score with more than five dices");
        } catch (TooManyDiceException success) {
            //if we caught the exception, this is success!
        }
    }
}
