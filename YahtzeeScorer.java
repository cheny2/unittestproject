import java.util.Arrays;

/**
 * This class calculates and stores the score for a yahtzee game.
 * The scores are calculated based on the scoring categories.
 * User can retrieve the current score, which is initialized to 0 at the
 * beginning of the game.
 * Created by cheny2 on 1/9/17.
 */
public class YahtzeeScorer {

    // creates an instance variable that denotes the current score
    private int score;
    // creates an instance variable that denotes whether the Yahtzee
    // scoring category has been scored
    private boolean yahtzeeScored;
    // creates an instance variable that denotes whether the Large Straight
    // scoring category has been scored
    private boolean largeStraightScored;
    // creates an instance variable that denotes whether the Full House
    // scoring category has been scored
    private boolean fullHouseScored;
    // creates an instance variable that denotes whether the Three Of A Kind
    // scoring category has been scored
    private boolean threeOfAKindScored;
    // creates an instance variable that denotes whether the user has already put
    // 0 in the Yahtzee box.
    private boolean zeroInYahtzee;

    /*
     * The constructor initializes the score to zero with none of the scoring
     * categories scored yet.
     */
    public YahtzeeScorer () {
        score = 0;
        yahtzeeScored = false;
        largeStraightScored = false;
        fullHouseScored = false;
        threeOfAKindScored = false;
        zeroInYahtzee = false;
    }

    /* Retrieves the current score for the user.
     * @return int the current score;
     */
    public int getScore() {
        return score;
    }

    /* Sets the current score to a given value.
    * @param int the new score;
    */
    public void setScore(int score) {
        this.score = score;
    }

    /*
     * Calculates the score. User should indicate a desired category to
     * score and an array of integers (representing the dices). The
     * method then judges whether the given dice values are appropriate to
     * score for that category. The scores for different categories are:
     *         Yahtzee (5 of a kind) scores 50 points;
     *         Large straight (5 values that form a sequence) scores 40 points;
     *         Full house (3 of one kind and 2 of another) scores 25 points;
     *         Three of a kind scores the sum of the five dices.
     * Note that each scoring category beside Yahtzee can only be scored once.
     * If the user tries to score the same category twice, the second scored would
     * not be added to the current score and the program would throw an exception.
     *
     * Also note that even if the dice values are not appropriate to score
     * for the given category, that category indicated would still be counted
     * as scored.
     *
     * @param String the desired score type
     * @param int[] the dice values
     * @throw AlreadyScoredException if the user tries to score the same category more than once
     *                               and that category is not Yahtzee
     * @throw NotEnoughDiceException if the user passes in an array of fewer than five dice values
     * @throw TooManyDiceException if the user passes in an array of more than five dice values
     * @return String the scenario - Normal Play if the user selects a category that
     *                                           has not yet been scored, and has the appropriate
     *                                           dice values for that category.
     *                              Insufficient Dice if the user selects a category that has not yet
     *                                                been scored, but does not have the appropriate dice
     *                                                values for that category
     *                              Already Scored if the user selects a category that has already been scored
     *                                             and that is not Yahtzee
     */
    public String evaluate(String scoreType, int[] dice) {
        if (dice.length < 5) {
            throw new NotEnoughDiceException("Should not be able to score with fewer than five dices");
        } else if (dice.length > 5) {
            throw new TooManyDiceException("Should not be able to score with more than five dices");
        }
        // Based on the desired scoring category the user passes in, the program
        // judges whether the user should score and how many points the user gains
        switch(scoreType) {
            case "Yahtzee":
                // The user gets both a bonus 100 points and a Yahtzee score (50 points)
                // for scoring each additional Yahtzee
                if (yahtzeeScored) {
                    if (zeroInYahtzee) {
                        setScore(score + 50);
                    } else {
                        setScore(score + 150);
                    }
                    return "Multiple Yahtzee!";
                } else {
                    return scoreYahtzee(dice);
                }
            case "Large Straight":
                if (largeStraightScored) {
                    throw new AlreadyScoredException("This category has already been scored!");
                } else {
                    return scoreLargeStraight(dice);
                }
            case "Full House":
                if (fullHouseScored) {
                    throw new AlreadyScoredException("This category has already been scored!");
                } else {
                    return scoreFullHouse(dice);
                }
            case "Three Of A Kind":
                if (threeOfAKindScored) {
                    throw new AlreadyScoredException("This category has already been scored!");
                } else {
                    return scoreThreeOfAKind(dice);
                }
                // An exception is thrown if the user passes in none of the four given
                // scoring categories.
            default:
                throw new NotValidScoringCategoryException("Should not be able to score a non-exist category");
        }
    }

    /*
     * Calculates the score for Yahtzee.
     *
     * @return String the scenario, either Normal Play or Insufficient Dice
     */
    private String scoreYahtzee(int[] dice) {
        yahtzeeScored = true;
        if (isYahtzee(dice)) {
            setScore(score + 50);
            return "Normal Play";
        } else {
            zeroInYahtzee = true;
            return "Insufficient Dice";
        }
    }

    /*
     * Determines whether the dice values are appropriate to score a Yahtzee.
     */
    private boolean isYahtzee(int[] dice) {
        boolean isYahtzee = (dice[0] == dice[1] && dice[0] == dice[2] && dice[0] == dice[3] && dice[0] == dice[4]);
        return isYahtzee;
    }

    /*
     * Calculates the score for Large Straight.
     * If the user tries to score Large Straight with the second or more Yahtzee
     * roll, s/he gets a bonus 100 points.
     * @return String the scenario, either Normal Play or Insufficient Dice or Multiple Yahtzee.
     */
    private String scoreLargeStraight(int[] dice) {
        largeStraightScored = true;
        if (isYahtzee(dice) && yahtzeeScored) {
            if (zeroInYahtzee) {
                setScore(score + 40);
            } else {
                setScore(score + 140);
            }
            return "Multiple Yahtzee!";
        }
        if (isLargeStraight(dice)) {
            setScore(score + 40);
            return "Normal Play";
        } else {
            return "Insufficient Dice";
        }
    }

    /*
     * Determines whether the dice values are appropriate to score a Large Straight
     * by sorting the dice value and then comparing one against the other.
     */
    private boolean isLargeStraight(int[] dice) {
        sortDice(dice);
        boolean isLargeStraight;
        isLargeStraight = ((dice[0] + 1 == dice[1]) && (dice[1] + 1 == dice[2])
                && (dice[2] + 1 == dice[3]) && (dice[3] + 1 == dice[4]));
        return isLargeStraight;
    }

    /*
     * Calculates the score for Full House.
     * If the user tries to score Full House with the second or more Yahtzee
     * roll, s/he gets a bonus 100 points.
     * @return String the scenario, either Normal Play or Insufficient Dice or Multiple Yahtzee.
     */
    private String scoreFullHouse(int[] dice) {
        fullHouseScored = true;
        if (isYahtzee(dice) && yahtzeeScored) {
            if (zeroInYahtzee) {
                setScore(25);
            } else {
                setScore(score + 125);
            }
            return "Multiple Yahtzee!";
        }
        if (isFullHouse(dice)) {
            setScore(score + 25);
            return "Normal Play";
        } else {
            return "Insufficient Dice";
        }

    }

    /*
     * Determines whether the dice values are appropriate to score a Full House
     * by sorting the dice value and then comparing one against the other.
     */
    private boolean isFullHouse(int[] dice) {
        sortDice(dice);
        if (dice[0] == dice[1] && dice[1] == dice[2]) {
            return (dice[3] == dice[4]);
        } else {
            return (dice[2] == dice[3] && dice[3] == dice[4]);
        }
    }

    /*
     * Calculates the score for Three-of-a-kind.
     * If the user tries to score Three-of-a-kind with the second or more Yahtzee
     * roll, s/he gets a bonus 100 points.
     * @return String the scenario, either Normal Play or Insufficient Dice or Multiple Yahtzee.
     */
    private String scoreThreeOfAKind(int[] dice) {
        threeOfAKindScored = true;
        if (isYahtzee(dice) && yahtzeeScored) {
            int sum = dice[0] + dice[1] + dice[2] + dice[3] + dice[4];
            if (zeroInYahtzee) {
                setScore(score + sum);
            } else {
                setScore(score + 100 + sum);
            }
            return "Multiple Yahtzee!";
        }
        if (isThreeOfAKind(dice)) {
            int sum = dice[0] + dice[1] + dice[2] + dice[3] + dice[4];
            setScore(score + sum);
            return "Normal Play";
        } else {
            return "Insufficient Dice";
        }
    }

    /*
     * Determines whether the dice values are appropriate to score a Three-of-a-kind
     * by sorting the dice value and then comparing one against the other.
     */
    private boolean isThreeOfAKind(int[] dice) {
        sortDice(dice);
        if (dice[0] == dice[1]) {
            return (dice[1] == dice[2]);
        } else {
            if (dice[1] == dice[2]) {
                return (dice[2] == dice[3]);
            } else {
                if (dice[2] == dice[3]) {
                    return (dice[3] == dice[4]);
                } else {
                    return false;
                }
            }
        }

    }

    /* Calls the built-in sorting mechanism to sort the dice values.*/
    private void sortDice(int[] dice) {
        Arrays.sort(dice);
    }
}
