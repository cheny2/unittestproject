/**
 * Created by cheny2 on 1/13/17.
 */
public class NotEnoughDiceException extends RuntimeException {
    public NotEnoughDiceException(String message) {
        super(message);
    }
}
