/**
 * Created by cheny2 on 1/13/17.
 */
public class AlreadyScoredException extends RuntimeException{
    public AlreadyScoredException(String message) {
        super(message);
    }
}
