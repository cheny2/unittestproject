/**
 * Created by cheny2 on 1/13/17.
 */
public class TooManyDiceException extends RuntimeException {
    public TooManyDiceException(String message) {
        super(message);
    }

}
